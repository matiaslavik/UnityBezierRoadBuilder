# Road builder tool for Unity, based on Bezier curves

Very simple road builder tool, based on Bezier curves.

Made it just for fun. I'm sure there are much better tools out there!

![Screenshot showing me building a road inside the Uinty Editor](Media/Screenshot1.png)
