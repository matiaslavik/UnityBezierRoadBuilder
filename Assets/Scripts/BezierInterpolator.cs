using UnityEngine;

public static class BezierInterpolator
{
    public static Vector3 GetPosition(Vector3 a, Vector3 b, Vector3 c, Vector3 d, float t)
    {
        float t0 = -1.0f * t * t * t + 3.0f * t * t - 3.0f * t + 1.0f;
        float t1 = 3.0f * t * t * t - 6.0f * t * t + 3.0f * t;
        float t2 = -3.0f * t * t * t + 3.0f * t * t;
        float t3 = t * t * t;
        return t0 * a + t1 * b + t2 * c + t3*d;
    }

    public static Vector3 GetDerivative(Vector3 a, Vector3 b, Vector3 c, Vector3 d, float t)
    {
        float t0 = -3.0f * t * t + 6.0f * t - 3.0f;
        float t1 = 9.0f * t * t - 12.0f * t + 3.0f;
        float t2 = -9.0f * t * t + 6.0f * t;
        float t3 = 3.0f * t * t;
        return t0 * a + t1 * b + t2 * c + t3*d;
    }
}
