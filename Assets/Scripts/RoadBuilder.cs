using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RoadBuilder : MonoBehaviour
{
    public Material material;
    public int numSegmentsPerCurve = 16;
    public float roadWidth = 1.0f;

    public List<RoadNode> nodes = new List<RoadNode>();

    private Mesh mesh;
    private List<Vector3> vertices = new List<Vector3>();
    private List<Vector2> uvs = new List<Vector2>();
    private List<int> triangles = new List<int>();
    private List<Vector3> curvePoints = new List<Vector3>();

    public const float roadSegmentLength = 4.0f;
    public const float controlPointSpacing = roadSegmentLength / 3.0f;

    void OnEnable()
    {
        mesh = new Mesh();
    }

    void Start()
    {
        
    }

    void Update()
    {
        // Create first road node if none
        if (nodes.Count < 1)
        {
            nodes = new List<RoadNode>{
                new RoadNode(){ position = Vector3.zero, rotation = Quaternion.identity },
                new RoadNode(){ position = Vector3.forward * roadSegmentLength, rotation = Quaternion.identity
            }};
        }

        // Create material if missing
        if (material == null)
            material = new Material(Shader.Find("Standard"));

        // Calculate Bezier curve points from road nodes
        curvePoints.Clear();
        curvePoints.Capacity = (nodes.Count - 1) * 4;
        for (int i = 1; i < nodes.Count; i++)
        {
            RoadNode prevNode = nodes[i-1];
            RoadNode currNode = nodes[i];
            curvePoints.Add(prevNode.position);
            curvePoints.Add(prevNode.position + prevNode.rotation * Vector3.forward * controlPointSpacing * prevNode.scale.z);
            curvePoints.Add(currNode.position - currNode.rotation * Vector3.forward * controlPointSpacing * currNode.scale.z);
            curvePoints.Add(currNode.position);
        }

        // Clear vertices
        vertices.Clear();
        uvs.Clear();
        // Reserve space for newvertex data
        vertices.Capacity = Mathf.Max(vertices.Capacity, numSegmentsPerCurve * curvePoints.Count / 4);
        uvs.Capacity = Mathf.Max(uvs.Capacity, numSegmentsPerCurve * curvePoints.Count / 4);

        for (int i = 0; i < curvePoints.Count; i+=4)
        {
            // Calculate the 4 control points of the current Bezier curve
            Vector3 p0 = curvePoints[i];
            Vector3 p1 = curvePoints[i+1];
            Vector3 p2 = curvePoints[i+2];
            Vector3 p3 = curvePoints[i+3];

            // Dicide curve into N discrete segments, for which we will create geometry
            float dist = 0.0f;
            for (int iSegment = 1; iSegment <= numSegmentsPerCurve; iSegment++)
            {
                // Calculate stard and end points of current segment
                float tStart = (iSegment - 1) / (float)numSegmentsPerCurve;
                float tEnd = iSegment / (float)numSegmentsPerCurve;
                Vector3 start = BezierInterpolator.GetPosition(p0, p1, p2, p3, tStart);
                Vector3 end = BezierInterpolator.GetPosition(p0, p1, p2, p3, tEnd);

                // Calculate derivative (direction)
                Vector3 startDerivative = BezierInterpolator.GetDerivative(p0, p1, p2, p3, tStart);
                Vector3 endDerivative = BezierInterpolator.GetDerivative(p0, p1, p2, p3, tEnd);

                // Calculate tangent (right vector)
                Vector3 startTangent = Vector3.Cross(startDerivative.normalized, Vector3.up);
                Vector3 endTangent = Vector3.Cross(endDerivative.normalized, Vector3.up);

                float halfWidth = roadWidth * 0.5f;

                vertices.Add(start - startTangent * halfWidth);
                vertices.Add(start + startTangent * halfWidth);
                vertices.Add(end - endTangent * halfWidth);
                vertices.Add(end + endTangent * halfWidth);

                float currDist = Vector3.Distance(start, end);
                float newDist = dist + currDist;
                uvs.Add(new Vector2(0.0f, dist));
                uvs.Add(new Vector2(1.0f, dist));
                uvs.Add(new Vector2(0.0f, newDist));
                uvs.Add(new Vector2(1.0f, newDist));
                dist = newDist;
            }
        }

        triangles.Capacity = Mathf.Max(triangles.Capacity, (vertices.Count - 2) * 3);
        triangles.Clear();

        for (int i = 2; i < vertices.Count; i+= 2)
        {
            triangles.Add(i-2);
            triangles.Add(i+1);
            triangles.Add(i);
            triangles.Add(i-1);
            triangles.Add(i+1);
            triangles.Add(i-2);
        }

        mesh.Clear();
        mesh.vertices = vertices.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.triangles = triangles.ToArray();

        Graphics.DrawMesh(mesh, transform.localToWorldMatrix, material, 0);
    }
}
