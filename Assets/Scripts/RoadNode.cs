using UnityEngine;
using System;

/// <summary>
/// A road node consists one centre point, surrounded by two control points.
/// The centre point will be the end point and start point of two connected bezier curves,
///  and the road will pass through this point.
/// The other two control points are positioned paralell so that a straight line can be drawn between the two points and the centre,
///  in order to ensure C1 continuity.
/// </summary>
[Serializable]
public class RoadNode
{
    // The position of the centre point
    public Vector3 position;
    // The rotation of all three points, around the centre point
    public Quaternion rotation;
    // The scale modifies the distance between the centre point and the two control points
    public Vector3 scale = Vector3.one;

    public Vector3 GetFirstControlPoint()
    {
        return position - rotation * Vector3.forward * RoadBuilder.controlPointSpacing * scale.z;
    }

    public Vector3 GetLastControlPoint()
    {
        return position + rotation * Vector3.forward * RoadBuilder.controlPointSpacing * scale.z;
    }
}
