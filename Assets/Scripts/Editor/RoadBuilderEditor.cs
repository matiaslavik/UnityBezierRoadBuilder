using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

[CustomEditor(typeof(RoadBuilder)), CanEditMultipleObjects]
public class RoadBuilderEditor : Editor
{
    private int selectedNodeIndex = -1;

    protected virtual void OnSceneGUI()
    {
        RoadBuilder roadBuilder = (RoadBuilder)target;

        // Show button handles for selecting road node
        for (int i = 0; i < roadBuilder.nodes.Count; i++)
        {
            RoadNode node = roadBuilder.nodes[i];
            if (Handles.Button(node.position, Quaternion.identity, 0.2f, 0.2f, Handles.SphereHandleCap))
            {
                selectedNodeIndex = i;
            }
        }

        // If node selected: Show transform handles for modifying node transforms
        if (selectedNodeIndex >= 0 && selectedNodeIndex < roadBuilder.nodes.Count)
        {
            RoadNode node = roadBuilder.nodes[selectedNodeIndex];
            EditorGUI.BeginChangeCheck();
            Vector3 position = node.position;
            Quaternion rotation = node.rotation;
            Vector3 scale = node.scale;
            Handles.TransformHandle(ref position, ref rotation, ref scale);
            if (EditorGUI.EndChangeCheck())
            {
                // Don't allow roll (rotation around Z) and scaling of the Y axis
                rotation = Quaternion.Euler(Vector3.Scale(rotation.eulerAngles, new Vector3(1.0f, 1.0f, 0.0f)));
                scale.y = 1.0f;
                Undo.RecordObject(roadBuilder, "Change road node");
                node.position = position;
                node.rotation = rotation;
                node.scale = scale;
            }
        }

        for (int i = 0; i < roadBuilder.nodes.Count; i++)
        {
            RoadNode node = roadBuilder.nodes[i];
            Vector3 start = node.GetFirstControlPoint();
            Vector3 end = node.GetLastControlPoint();
            Color oldColour = Handles.color;
            Handles.color = Color.green;
            // Draw control points
            Handles.SphereHandleCap(10, start, Quaternion.identity, 0.1f, EventType.Repaint);
            Handles.SphereHandleCap(11, end, Quaternion.identity, 0.1f, EventType.Repaint);
            // Draw line between points
            Handles.DrawDottedLine(start, end, 2.0f);
            Handles.color = oldColour;
        }
    }

    public override void OnInspectorGUI()
    {
        RoadBuilder roadBuilder = (RoadBuilder)target;

        if (GUILayout.Button("Add node"))
        {
            Undo.RecordObject(roadBuilder, "Add road node");
            RoadNode lastNode = roadBuilder.nodes[roadBuilder.nodes.Count - 1];
            Vector3 direction = lastNode.rotation * Vector3.forward;
            roadBuilder.nodes.Add(new RoadNode(){
                position = lastNode.position + direction * RoadBuilder.roadSegmentLength,
                rotation = lastNode.rotation});
            selectedNodeIndex = roadBuilder.nodes.Count - 1;
            SceneView.RepaintAll();
        }
        if (GUILayout.Button("Remove node"))
        {
            Undo.RecordObject(roadBuilder, "Remove road node");
            if (roadBuilder.nodes.Count > 1)
                roadBuilder.nodes.RemoveAt(roadBuilder.nodes.Count - 1);
            selectedNodeIndex = -1;
            SceneView.RepaintAll();
        }
        if (GUILayout.Button("Clear"))
        {
            Undo.RecordObject(roadBuilder, "Clear road nodes");
            roadBuilder.nodes = new List<RoadNode>{
                new RoadNode(){ position = Vector3.zero, rotation = Quaternion.identity },
                new RoadNode(){ position = Vector3.forward * RoadBuilder.roadSegmentLength, rotation = Quaternion.identity }};
            selectedNodeIndex = -1;
            SceneView.RepaintAll();
        }

        this.DrawDefaultInspector();
    }
}
